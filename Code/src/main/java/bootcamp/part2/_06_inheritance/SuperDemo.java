package bootcamp.part2._06_inheritance;

public class SuperDemo {

	public static void main(String[] args) {
		new B();
	}
}

class A {
	private int n;
	A(int n) {
		this.n = n;
		System.out.println("create a new A with n = " + n);
	}
	A() {
		new A(33);
	}
	int compute() {
		return (int)(Math.random()*10);
	}
}

class B extends A {
	B() {
		System.out.println("create a new B");

		System.out.println(compute());
		System.out.println(this.compute());
		System.out.println(super.compute());
	}
	int compute() {
		return 100;
	}
}
