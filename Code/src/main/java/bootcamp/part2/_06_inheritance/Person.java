package bootcamp.part2._06_inheritance;

public class Person {
	protected String name;
	protected boolean isIn;
	Person(String name) {
		this.name = name;
	}
	boolean inIn() {
		return isIn;
	}
	void enter() {
		isIn = true;
		System.out.print(name + " stepped in. ");
	}
	void exit() {
		isIn = false;
		System.out.print(name + " stepped out. ");
	}
}
