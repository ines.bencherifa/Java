package bootcamp.part2._06_inheritance;

public class Document {

	protected String title;
	protected String publisher;

	Document(String title, String publisher) {
		this.title = title;
		this.publisher = publisher;
	}

	public void getInfo() {
		System.out.print("Title: " + title + ", Publisher: " + publisher);
	}

}
