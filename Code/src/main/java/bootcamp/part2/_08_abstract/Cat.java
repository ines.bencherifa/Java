package bootcamp.part2._08_abstract;

public class Cat extends Animal {

	protected Cat(int age) {
		super(age);
	}

	@Override
	String getSound() {
		return "meow!";
	}

}
