package bootcamp.part1._03_methods;

public class MethodWithParamAndReturn {

	public static void main(String[] args) {
		new MethodWithParamAndReturn();
	}

	MethodWithParamAndReturn() {
		System.out.println(atLeastOneFalse(true, true, true));
		System.out.println(atLeastOneFalse(true, true, false));
		System.out.println(atLeastOneFalse(false, true, true));
	}

	boolean atLeastOneFalse(boolean a, boolean b, boolean c) {
		return !a || !b || !c;
	}
}
