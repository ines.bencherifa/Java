package bootcamp.part1._04_cast;

public class CastExample {

	public static void main(String[] args) {
		new CastExample();
	}
	
	CastExample() {
		
		// OK: an int is a double with no decimal part:
		int i = 3;
		double d = i;
		
		// not OK: an int is more restrictive than a double:
		/*
		double pi = 3.14;
		int n = pi;
		System.out.println(n);
		*/
		
		// acknowledge the precision loss with a cast:
		double pi = 3.14;
		int n = (int)pi;
		
		double db = n;
		System.out.println("pi=" + pi + " n=" + n + " db=" + db);
	}
}
