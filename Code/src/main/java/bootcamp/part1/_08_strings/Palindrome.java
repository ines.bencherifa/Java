package bootcamp.part1._08_strings;

public class Palindrome {

	public static void main(String[] args) {
		new Palindrome();
	}
	
	Palindrome() {
		System.out.println(isPalindrome("kayak")); //true
		System.out.println(isPalindrome("Was it a car or a cat I saw?")); // true
		System.out.println(isPalindrome("foo")); // false
	}

	boolean isPalindrome(String s) {
		boolean palindrome = true;
		s = s.replace(" ", "");
		s = s.replace("?", "");
		s = s.replace(".", "");
		s = s.replace(",", "");
		s = s.replace(";", "");
		s = s.replace(":", "");
		s = s.replace("!", "");
		s = s.toLowerCase();
		for (int i = 0; i < s.length() / 2; i++) {
			if (s.charAt(i) != s.charAt(s.length() - 1 - i))
				palindrome = false;
		}
		return palindrome;
	}

}
