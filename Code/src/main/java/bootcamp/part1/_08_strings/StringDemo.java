package bootcamp.part1._08_strings;

public class StringDemo {

	public static void main(String[] args) {
		new StringDemo();
	}
	
	StringDemo() {
		String a = "kayak";
	    String b = "kayak";
	    // Strings are reused, a and b points to the same memory area:
	    System.out.println("a: " + a + "\t b: " + b + "\t a == b ?  " + (a == b)); // true
	    // a and b contains the same characters:
	    System.out.println("a: " + a + "\t b: " + b + "\t a.equals(b) ?  " + a.equals(b)); // true

	    String c = a; // c explicitly points to the same String as a
	    System.out.println("a: " + a + "\t c: " + c + "\t a == c ?  " + (a == c));  // true
	    System.out.println("a: " + a + "\t c: " + c + "\t a.equals(c) ?  " + a.equals(c)); // true
	    
	    a = a + " is fun";
	    // Java strings are immutable: a new String is created for a:
	    System.out.println("a: " + a); // kayak is fun
	    // c keeps pointing to the initial string:
	    System.out.println("c: " + c); // kayak
	    // a and b don't point to the same memory area now:
	    System.out.println("a: " + a + "\t b: " + b + "\t a == b ?  " + (a == b));
	    // a and b don't contain the same characters:
	    System.out.println("a: " + a + "\t b: " + b + "\t a.equals(b) ?  " + a.equals(b));
	    
	    a = "kayak"; // since the string kayak already exists, a points to it
	    // so now a, b and c point to the same memory area and contain the same characters
	    System.out.println("a: " + a + "\t b: " + b + "\t a == b ?  " + (a == b));
	    System.out.println("a: " + a + "\t b: " + b + "\t a.equals(b) ?  " + a.equals(b));
	    System.out.println("a: " + a + "\t c: " + c + "\t a == c ?  " + (a == c));
	    System.out.println("a: " + a + "\t c: " + c + "\t a.equals(c) ?  " + a.equals(c));  
	}
}
